## Explications pour comment mettre en degrés Celsius 

### Démarche à suivre
>- Ouvrir l'IDE Arduino (le programme qui permet de programmer les Arduino).
>- Faire 'Nouveau' dans le menu Fichier'
>- Copier le code d'origine se trouvant [->ici<-](https://gitlab.com/AmbassadAir/AirBeam/blob/master/Arduino/AirBeamFirmware_11_14_15.ino).
>- Coller ce code à la place du code se trouvant dans la fenêtre de programmation ouverte précédemment.
>- Modifier les lignes 212 à 216 en les mettant en commentaires (à l'aide des //).
>- Modifier les lignes 218 à 222 en supprimant les // pour les décommenter.
>- Aller dans le menu 'Outils' et Sélectionner 'Type de Carte:', choisir 'Arduino Leonardo'.
>- Toujours dans le menu 'Outils', sélectionner le port qui contient le nom 'Leonardo'. Normalement, il ne doit y avoir qu'un seul port avec ce mot.
>- Vérifier que le boîtier AirBeam est bien sous tension (la led rouge doit clignoter).
>- Téléverser le programme (flèche horizontale en haut de la fenêtre de programmation ou menu 'Croquis' et 'Téléverser'.
>- Attendre la fin de l'opération, votre programme vient d'être mis à jour.

### Quelques images pour aider

Le code d'origine
![alt tag](https://gitlab.com/AmbassadAir/AirBeam/uploads/3096c16269e0325ffa9f3c85754f285d/Capture_d_écran_2017-02-22_à_10.06.06.png)


Le code modifié
![alt tag](https://gitlab.com/AmbassadAir/AirBeam/uploads/f1ed11ac30f4d3e8c6e32f83fc91fe3d/Capture_d_écran_2017-02-22_à_10.43.52.png)
